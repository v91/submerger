1
00:05:40,037 --> 00:05:42,122
Inside. In the kitchen.

2
00:09:02,823 --> 00:09:03,741
Are you all right?

3
00:09:04,366 --> 00:09:06,577
An accident. Sorry to trouble you.

4
00:09:07,327 --> 00:09:08,162
May I come in?

5
00:09:08,162 --> 00:09:09,204
Of course!

6
00:09:14,376 --> 00:09:16,086
Sugar, milk?

7
00:09:17,087 --> 00:09:18,630
No need. Thank you.

8
00:09:35,939 --> 00:09:36,857
Delicious!

9
00:09:39,109 --> 00:09:40,069
What's wrong?

10
00:09:40,069 --> 00:09:41,695
Don Eduardo is here!

11
00:09:44,615 --> 00:09:45,908
So good to see you!

12
00:09:46,366 --> 00:09:47,910
I'd shake your hand, but...

13
00:09:47,910 --> 00:09:50,370
Look at you! Mountain man!

14
00:09:51,288 --> 00:09:53,373
You look like ZZ Top!

15
00:09:55,709 --> 00:09:58,087
Clean up for Don Eduardo! Go wash, shave!

16
00:09:58,087 --> 00:09:59,171
Oh, sure!

17
00:09:59,671 --> 00:10:01,298
I'm sorry, Don Eduardo.

18
00:10:01,799 --> 00:10:02,800
Go!

19
00:10:08,263 --> 00:10:10,265
Keep the mustache and the soul patch.
It looks good.

20
00:10:11,558 --> 00:10:13,227
Yes, Don Eduardo.

21
00:10:15,312 --> 00:10:17,898
You're sure you don't want breakfast?

22
00:10:17,898 --> 00:10:19,858
The eggs are fresh from this morning.

23
00:10:22,611 --> 00:10:24,446
How are Mateo's teeth doing? Better?

24
00:10:24,988 --> 00:10:28,033
Yes! Your dentist worked so hard!

25
00:10:28,117 --> 00:10:32,746
Matti used to be up all night
with the pain.

26
00:10:32,830 --> 00:10:37,459
Now he sleeps like a baby.
Thanks to you, Don Eduardo.

27
00:10:38,085 --> 00:10:39,545
That's good, Sylvia.

28
00:10:39,545 --> 00:10:41,046
And the goats?

29
00:10:41,547 --> 00:10:44,133
From your hometown?
They don't miss the mountains?

30
00:10:44,133 --> 00:10:47,928
They're fine.
We're expecting two more next month.

31
00:10:47,928 --> 00:10:49,513
You sent for more?

32
00:10:51,056 --> 00:10:53,016
Cabritos. Babies.

33
00:10:54,810 --> 00:11:00,440
You're almost finished. I'll make more.
You're sure about breakfast?

34
00:11:01,150 --> 00:11:02,359
Just coffee is fine.

