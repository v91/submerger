1
00:00:08,833 --> 00:00:14,916
Black holes are considered to be
the hellmouths of the universe.

2
00:00:15,625 --> 00:00:19,083
Those who fall inside disappear.

3
00:00:19,791 --> 00:00:20,625
Forever.

4
00:00:25,000 --> 00:00:26,625
But whereto?

5
00:00:28,083 --> 00:00:31,583
What lies behind a black hole?

6
00:00:31,666 --> 00:00:33,375
WHERE (WHEN) IS MIKKEL?

7
00:00:33,458 --> 00:00:37,375
Along with things,
do space and time also vanish there?

8
00:00:37,458 --> 00:00:40,458
Or would space and time

9
00:00:40,541 --> 00:00:44,291
be tied together
and be part of an endless cycle?

10
00:00:44,916 --> 00:00:48,333
What if everything that came from the past

11
00:00:49,041 --> 00:00:52,875
were influenced by the future?

12
00:00:54,041 --> 00:00:55,458
Tick-tock.

13
00:00:55,958 --> 00:00:57,500
Tick-tock.

14
00:00:58,083 --> 00:00:59,333
Tick-tock.

