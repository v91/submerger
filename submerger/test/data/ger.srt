1
00:00:08,833 --> 00:00:14,916
[Wissenschaftler]  Schwarze Löcher gelten
als die Höllenschlunde des Universums.

2
00:00:15,625 --> 00:00:19,708
Wer hineinfällt, verschwindet.

3
00:00:19,791 --> 00:00:21,458
Für immer.

4
00:00:25,000 --> 00:00:26,625
Aber wohin?

5
00:00:28,083 --> 00:00:31,583
Was liegt hinter einem schwarzen Loch?

6
00:00:32,375 --> 00:00:37,375
Verschwinden dort mit den Dingen
auch Raum und Zeit?

7
00:00:37,458 --> 00:00:40,375
Oder wären Raum und Zeit

8
00:00:40,458 --> 00:00:44,291
in einem ewigen Kreis
miteinander verbunden?

9
00:00:44,833 --> 00:00:48,333
Und was, wenn das,
was aus der Vergangenheit kommt,

10
00:00:49,041 --> 00:00:52,875
durch die Zukunft beeinflusst wäre?

11
00:00:54,041 --> 00:00:55,916
Tick, tack.

12
00:00:56,000 --> 00:00:57,500
Tick, tack.

13
00:00:58,083 --> 00:00:59,791
Tick, tack.

